// Helpers/chiensData.js

const data = [{
      id: 1,
      name: "Labrador",
      desc: "Le retriever du Labrador, plus communément appelé labrador retriever ou plus simplement labrador, est une race de chiens originaire du Royaume-Uni. ",
      color: 'Noire, marron ou jaune',
      character: ''
   },
   {
      id: 2,
      name: "Basset fauve de Bretagne",
      desc: "Le basset fauve de Bretagne est une race de chien de chasse originaire de Bretagne. ",
      color: 'Fauve unicolore.',
      character: 'Sociable, affectueux, équilibré.'
   },
   {
      id: 3,
      name: "Beagle",
      desc: "Le beagle  est une race de chien originaire d’Angleterre, de taille petite à moyenne.",
      color: '',
      character: ''
   }
]

export default data