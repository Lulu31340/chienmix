import axios from 'axios'

export function getChiens() {
    const url = "http://chienmix.tk/api/chiens"
    return new Promise((resolve,reject) => {
        return axios.get(url).then(response => {
            resolve(response)
        }).catch(error => {
            reject(error)
        });
    });
}