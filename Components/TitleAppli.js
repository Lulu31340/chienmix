import React from "react";
import { View , Text} from "react-native";
import { StyleSheet } from "react-native";

class TittleAppli extends React.Component{
    render(){
        return(
            <View >
                <Text style={styles.titre}>ChienMix</Text>
                <Text style={styles.text}>L'application pour mixer les Chiens</Text>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    titre:{
      fontSize:40,
      textAlign:'center'
    },
    text:{
        fontSize:20,
        textAlign:'center'
      }
  });

export default TittleAppli