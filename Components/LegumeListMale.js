import React from "react";
import { View , Text, FlatList} from "react-native";
import { StyleSheet } from "react-native";
import ChienItem from "./ChienItem.js";
import { getChiens} from './../API/chienAPI.js';

class TittleAppli extends React.Component{
    constructor(props) {
        super(props)
        this.state = { chiens: []}
    }
    componentDidMount(){
        getChiens().then(res => {
            this.setState({ chiens: res.data})
            this.forceUpdate()
        }).catch(err =>{
            console.log(err)
        })
    }
    render(){
        return(
            <View style={styles.container}>
                <FlatList data={this.state.chiens}
                    renderItem={({item})=> <ChienItem chien={item}></ChienItem> }>
                </FlatList>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: '#F08080',
        flexDirection : 'row',
    },
  });

export default TittleAppli