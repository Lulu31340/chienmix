import React from "react";
import { View , Text, Image, Button} from "react-native";
import { StyleSheet } from "react-native";

class ChienItem extends React.Component{
    render(){
      const chien = this.props.chien
        return(
            <View style={styles.container}>
                <View style={styles.photo}>
                    <Image style={styles.photo} source={{uri: chien.imageUrl,}}/>
                </View>
                <View style={styles.partiedroite}>
                    <View style={styles.nom}>
                        <Text>{chien.name}</Text>
                    </View>
                    <View style={styles.btn}>
                        <Button title="+ infos" color={"white"}></Button>
                    </View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex:1,
        flexDirection : 'row',
        margin :10,
      },
      photo:{
        height:100,
        flex:3,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'black',
      },
      partiedroite:{
        flex:7,
        backgroundColor: '#E65F5C',
        borderRadius: 15,
      },
      nom:{
        flex:2,
        backgroundColor: '#E65F5C',
        justifyContent: 'center',
        alignItems:"center",
      },
      btn:{
        flex:1,
        backgroundColor: '#FE4A49',
        borderRadius:100,
        justifyContent: 'center',
        alignSelf:"center",
      },
  });

export default ChienItem