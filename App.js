import { StyleSheet, Text, View } from 'react-native';
import TittleAppli from './Components/TitleAppli';
import LegumeListMale  from './Components/LegumeListMale';


export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.bloc10}><TittleAppli></TittleAppli></View>
      <View style={styles.bloc70}><LegumeListMale></LegumeListMale></View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#fff',
  },

  bloc70:{
    flex:7,
    backgroundColor: 'blue',
  },

  bloc40:{
    flex:4,
    backgroundColor: 'brown',
  },

  bloc10:{
    flex:1,
    backgroundColor: 'red',
  }
});
